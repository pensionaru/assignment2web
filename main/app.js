function addTokens(input, tokens) {
  let flag = false;
  if (!(typeof input === "string")) {
    throw new Error("Invalid input");
  } else if (input.length < 6) {
    throw new Error("Input should have at least 6 characters");
  }

  for (let i = 0; i < tokens.length; i++) {
    if (typeof tokens[i].tokenName !== "string") {
      throw new Error("Invalid array format");
    }
  }

  if (input.indexOf("...") == -1) {
    return input;
  } else {
    let stringArray = input.split("...");

    for (let i = 0; i < stringArray.length; i++) {
      if (typeof tokens[i] != "undefined") {
        stringArray[i] += "${" + tokens[i].tokenName + "}";
      }
    }
    return stringArray.join("");
  }
}

const app = {
  addTokens: addTokens
};

module.exports = app;
